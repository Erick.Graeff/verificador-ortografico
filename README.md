# Verificador Ortografico


Este projeto implementa um verificador ortográfico simples, que recebe um texto lido da entrada padrão *(stdin)* e reproduz fielmente o texto (inclusive espaços e quebras de linha) na saída padrão *(stdout)*, com as palavras incorretas indicadas entre colchetes. Uma palavra é considerada incorreta se não for encontrada em um arquivo de dicionário pré-definido.

Consideram-se como palavras as sequências contínuas de letras (A-Z, a-z) com ou sem acentos e as cedilhas; os demais caracteres (números, espaços e outros símbolos) não fazem parte de palavras.

Por exemplo, para esta entrada:

> Para que o pocessador possa interromper a execução de uma tarefa
> e retornar a ela mais tarde, sem corromper seu estado interno,
> é necessário definir operações para salvar e restaurrar o
> contexto da tarefa.
> 
> O ato de salvar os valores do contexto atual em seu TCB e
> possivelmente restaurar o contexto de outra tarefa, previamente
> salvo em outro TCB, é denominado "troca de contexto".

O programa gera esta saída:

> Para que o [pocessador] possa interromper a execução de uma tarefa
> e retornar a ela mais tarde, sem corromper seu estado interno, 
> é necessário definir operações para salvar e [restaurrar] o
> contexto da tarefa.
> 
> O ato de salvar os valores do contexto atual em seu [TCB] e
> possivelmente restaurar o contexto de outra tarefa, previamente
> salvo em outro [TCB], é denominado "troca de contexto".

Forma de chamada: <br>
```./ortografia < entrada.txt > saida.txt```

## Arquivos
### ortografia.c:
Arquivo principal do projeto. Utiliza a função ```setlocale()``` para definir que as variáveis do programa serão do tipo especificado (ISO-8859-1). Após isso, abre o arquivo de dicionário com a opção "read" e realiza as operações necessárias para atender a especificação do projeto;

---
### dicionario.c:
Funções relativas ao dicionário (carregar o dicionário na memória, verificar se uma palavra está no dicionário, etc);

---
### dicionario.h:
Arquivo de cabeçalho das funções de dicionario.c. Além de implementar estrutura utilizada no programa;

---
### Makefile:
Para compilar o projeto basta rodar ```make```. Rodando ```make clean``` os arquivos temporários serão removidos. Rodando ```make purge``` tudo que não for o código principal será removido.

---
### Arquivos de teste:
> plutao.txt (2219 bytes); <br>
> memoria.txt (3121 bytes); <br>
> brascubas.txt (351.233 bytes); <br>
> montesquieu.txt (1.253.594 bytes);

Os arquivos acima e o dicionário estão no formato ISO-8859-1, que usa um byte por caractere para representar letras e sinais gráficos na tabela ASCII estendida (com 256 caracteres). Se desejar utilizar outros textos, garanta que também estejam no formato ISO-8859-1.

---
### Arquivos de saída:
> plutao-saida.txt; <br>
> memoria-saida.txt; <br>
> brascubas-saida.txt; <br>
> montesquieu-saida.txt;

Os arquivos de teste presentes em "**Arquivos de teste**" geram, respectivamente, as saídas citadas acima, podem ser usados para verificar se o código está correto, caso deseje alterar o código fonte.
Exemplo:
```
./ortografia < plutao.txt > new-plutao-saida.txt
diff new-plutao-saida.txt plutao-saida.txt
```
O comando diff não deve retornar nada.