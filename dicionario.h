// GRR20193175 Erick Graeff Petzold

#ifndef __DICIONARIO__
#define __DICIONARIO__

#include <stdio.h>

typedef struct dictionary_t {
    int word;
    char **dictionary;
    int size;
    int space;
} Tp_dic;

void allocate_dictionary (FILE *arq, Tp_dic *d);

void receive_words (FILE *arq, Tp_dic *d);

void allocate_space (FILE *arq, Tp_dic *d);

void free_dictionary (Tp_dic *d);

void lower_case (Tp_dic *d);

void config_dictionary (Tp_dic *d);

void check_input (Tp_dic *d);

#endif