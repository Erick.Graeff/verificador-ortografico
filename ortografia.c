// GRR20193175 Erick Graeff Petzold

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "dicionario.h"

int main () {

    char *locale;
    locale = setlocale (LC_CTYPE, "pt_BR.ISO-8859-1");
    if (!locale) {
        fprintf (stderr, "Can't set the specified locale\n");
        exit (1);
    }

    FILE* arquivo;
    arquivo = fopen ("brazilian", "r");

    if (!arquivo) {
        perror ("Erro ao abrir arquivo brazilian");
        exit (1);
    }

    Tp_dic dictionary;

    allocate_dictionary (arquivo, &dictionary);
    receive_words (arquivo, &dictionary);
    fclose (arquivo);
    config_dictionary (&dictionary);
    check_input (&dictionary);
    
    free_dictionary (&dictionary);
    return 0;
}