// GRR20193175 Erick Graeff Petzold

#include "dicionario.h"
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define WORD_SIZE 20
#define AMOUNT_OF_WORDS 10000

/* caso nao tenha espaco, encerra programa com codigo de erro */
void allocate_dictionary (FILE *arq, Tp_dic *d) {
    int i;

    d->dictionary = calloc (AMOUNT_OF_WORDS + 1, sizeof (char*));
    if (d->dictionary == NULL) {
        printf ("Memória insuficiente, não foi possível continuar\n");
        fclose (arq);
        exit (1);
    }

    for (i = 0; i < AMOUNT_OF_WORDS; i++) {
        d->dictionary [i] = calloc (WORD_SIZE + 1, sizeof (char));
        if (d->dictionary [i] == NULL) {
            printf ("Memória insuficiente, não foi possível continuar\n");
            fclose (arq);
            exit (1);
        }
    }
    d->space = i;
}

void receive_words (FILE *arq, Tp_dic *d) {
    d->word = 0;
    fscanf (arq, "%s", d->dictionary [d->word]);
    
    while (!feof (arq)) {
        d->word++;

        /* Se nao houver espaco para adicionar palavras, aloca mais */
        if (d->word == d->space)
            allocate_space (arq, d);
        
        fscanf (arq, "%s", d->dictionary [d->word]);
    }
    d->size = d->word;
}

/* caso nao tenha espaco, encerra programa com codigo de erro */
void allocate_space (FILE *arq, Tp_dic *d) {
    int i;

    d->dictionary = (char **) realloc (d->dictionary, (d->space + AMOUNT_OF_WORDS) * sizeof (char*));
    if (d->dictionary == NULL) {
        printf ("Memória insuficiente, não foi possível continuar\n");
        fclose (arq);
        exit (1);
    }

    for (i = d->space; i < d->space + AMOUNT_OF_WORDS; i++) {
        d->dictionary [i] = calloc (WORD_SIZE, sizeof (char));
        if (d->dictionary [i] == NULL) {
            printf ("Memória insuficiente, não foi possível continuar\n");
            fclose (arq);
            exit (1);
        }
    }
    d->space = i;
}

void free_dictionary (Tp_dic *d) {
    int i;

    for (i = 0; i < d->space; i++)
        free (d->dictionary [i]);
    free (d->dictionary);
}

void lower_case (Tp_dic *d) {
    d->word = 0;
/* se for uma letra maiuscula, transforma em minuscula */
    while (d->word <= d->size) {
        if (isupper (d->dictionary [d->word][0]))
            d->dictionary [d->word][0] = tolower (d->dictionary [d->word][0]);
        d->word++;
    }
}

int compare (const void *a, const void* b) {
    const char *string_1 = *(const char **) a;
    const char *string_2 = *(const char **) b;

    return strcasecmp (string_1, string_2);
}

void config_dictionary (Tp_dic *d) {
    lower_case (d);
    qsort (d->dictionary, d->size, sizeof (char*), compare);
}

void check_input (Tp_dic *d) {
    char *input, c, **in_dictionary;
    int i;

    input = calloc (WORD_SIZE + 1, sizeof (char));
    c = getchar ();
    while (!feof (stdin)) {
        /* enquanto nao for palavra e nem fim da entrada */
        while (!isalpha (c) && !feof (stdin)) {
            putchar (c);
            c = getchar ();
        }
        i = 0;
        /* enquanto for uma palavra e não for fim da entrada */
        while (isalpha (c) && !feof (stdin)) {
            input [i] = c;
            c = getchar ();
            i++;
        }
        input [i] = '\0';

        in_dictionary = (char **) bsearch (&input, d->dictionary, d->size, sizeof (char*), compare);
        if (!in_dictionary && !feof (stdin))
            printf ("[%s]", input);
        else
            printf ("%s", input);
    }
    free (input);
}