# GRR20193175 Erick Graeff Petzold

objs = ortografia.o dicionario.o
  
all: ortografia

ortografia: ortografia.c dicionario.c dicionario.h
	gcc -Wall -o ortografia ortografia.c dicionario.c

clean:
	-rm -f $(objs) *~
		 
# remove tudo o que não for o código-fonte
purge: clean
	-rm -f ortografia
